The Sun God Collective
=====
*Sometimes I listen to music. Below is a summary of the music that I have listend to over the years.*

Artists
-----
### Jazz
+ Paper Void
+ Nujabes
+ BADBADNOTGOOD
+ Snarky Puppy
+ John Coltrane
+ Miles Davis
+ Thelonious Monk
+ The Seatbelts

### Hip hop
+ Chance the Rapper
+ Childish Gambino
+ Danger Doom
+ Run the Jewels
+ Towkio
+ Donnie Trumpet & The Social Experiment
+ J. Cole
+ Toro y Moi

### Electro-something
+ Bonobo
+ Crystal Castles
+ Vulfpeck
+ Baths

### Pop
+ Beach House
+ Charlie Lim

### Rock of all types
+ Local Natives
+ The Flaming Lips
+ THR!!!ER
+ Cold War Kids
+ STRFKR
+ Thank You Scientist
+ Whirr
+ Broken Back
+ Iron & Wine
+ Working for a Nuclear Free City
+ Fleet Foxes
+ HINTO

### Acoustic dubstep (aka Cave Music)
+ Moon Hooch

### Metals
+ Consider The Source
+ Meshuggah

### Instrumental
+ Jake Shimabukuro (ukulele)
+ Chris Thile (mandolin)

### Guilty pleasures
+ 3OH!3
+ Kyary Pamyu Pamyu
+ Pink Season
+ Rebecca Sugar
